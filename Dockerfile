
FROM ubuntu:18.04 as dev

#prevent prompts during installs
ENV DEBIAN_FRONTEND=noninteractive

#passwords as arguments so they can be changed
ARG DEV_PW=password
ARG JENKINS_PW=jenkins

ENV LC_CTYPE en_US.UTF-8
ENV LANG en_US.UTF-8



# Install system dependencies
RUN apt update -qq && apt install -qq -y --no-install-recommends \
        git \
        build-essential \
        wget \
        make \
        cmake \ 
        python3\ 
        python3-pip \
        python3-setuptools \
        plantuml \
        graphviz \
        gdb \
        locales \
        libusb-1.0-0-dev \
        libtool \
        autotools-dev \
        automake \
        libboost-all-dev flex bison texinfo unzip openssl ca-certificates tar 




#Copy and unzip patched STM8S StdPeriph library 
ADD STM8S_StdPeriph_Lib-patched.tar.gz /opt/


## Install STM8 flash
RUN git clone https://github.com/vdudouyt/stm8flash.git && \
    cd stm8flash  &&  \
    make -j &&   \
    make install

ADD 49-stlinkv2.rules /etc/udev/rules.d/


#Build and install openocd 
RUN git clone https://github.com/openocd-org/openocd.git && \
    cd openocd && \
    git checkout tags/v0.11.0 && \
    ./bootstrap && \ 
    ./configure && \
    make -j && \ 
    make install 

#Create ENV variable for OpenOCD script location
ENV OPENOCD_SCRIPTS=/usr/local/share/openocd/scripts


#Build and install SDCC 
RUN wget -O /tmp/sdcc-3.7.0.tar.bz2 "https://sourceforge.net/projects/sdcc/files/sdcc/3.7.0/sdcc-src-3.7.0.tar.bz2/download" && \
    tar xvf /tmp/sdcc-3.7.0.tar.bz2 -C /tmp && \
    rm -fr /tmp/sdcc-3.7.0.tar.bz2 && \
    cd /tmp/sdcc && \
    ./configure \
      --disable-mcs51-port \
      --disable-z80-port \
      --disable-z180-port \
      --disable-r2k-port \
      --disable-r3ka-port \
      --disable-gbz80-port \
      --disable-tlcs90-port \
      --disable-ds390-port \
      --disable-ds400-port \
      --disable-pic14-port \
      --disable-pic16-port \
      --disable-hc08-port \
      --disable-s08-port && \
    make -j && \
    make install && \
    rm -fr /tmp/sdcc 

RUN apt install -qq -y --no-install-recommends babeltrace

#install stm8-binutils for debugging 
#
# For some reason this fails when it is a single RUN command, so it is split into 3
#
RUN wget -O /tmp/stm8-binutils-gdb-sources-2021-07-18.tar.gz   "https://sourceforge.net/projects/stm8-binutils-gdb/files/stm8-binutils-gdb-sources-2021-07-18.tar.gz/download" && \
    tar xvf /tmp/stm8-binutils-gdb-sources-2021-07-18.tar.gz -C /tmp && \
    rm -rf /tmp/stm8-binutils-gdb-sources-2021-07-18.tar.gz && \
    cd /tmp/stm8-binutils-gdb-sources && \ 
    ./patch_binutils.sh 

RUN cd /tmp/stm8-binutils-gdb-sources/binutils-2.30 && \
    ./configure --host=$(./config.guess) --target=stm8-none-elf32 --program-prefix=stm8- --without-auto-load-safe-path &&\
    make

RUN cd /tmp/stm8-binutils-gdb-sources/binutils-2.30 && \
    make install && \
    rm -rf /tmp/stm8-binutils-gdb-sources


# Add user dev to the image
RUN adduser --quiet dev && \
    echo "dev:$DEV_PW" | chpasswd && \
    chown -R dev /home/dev 


RUN pip3 install -U mrtutils

#Copy the latest release of plantuml. Ubuntu 18.04 package manager has an older one without timing diagram support
RUN wget https://github.com/plantuml/plantuml/releases/download/v1.2022.1/plantuml-1.2022.1.jar -O /usr/share/plantuml/plantuml.jar

RUN python3 -m pip install -U --force-reinstall pip



#sphinx dependencies 
RUN pip3 install wheel sphinx sphinxcontrib-plantuml sphinx-rtd-theme recommonmark restview docxbuilder docxbuilder[math]


#There is a breaking change in the new version of markupsafe. This is a temporary workaround
RUN pip3 install markupsafe==2.0.1

######################################################################################################
#                           Stage: jenkins                                                           #
######################################################################################################
FROM dev as jenkins

ARG JENKINS_PW

ENV LC_CTYPE en_US.UTF-8
ENV LANG en_US.UTF-8


#install jenkins dependencies
RUN apt install -qq -y --no-install-recommends openjdk-8-jdk  openssh-server ca-certificates
RUN adduser --quiet jenkins && \
    echo "jenkins:$JENKINS_PW" | chpasswd && \
    mkdir /home/jenkins/.m2 && \
    mkdir /home/jenkins/jenkins && \
    chown -R jenkins /home/jenkins 
#JENKINS END

# Setup SSH server
RUN mkdir /var/run/sshd
RUN echo 'root:password' | chpasswd
RUN sed -i 's/#*PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed -i 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' /etc/pam.d/sshd

ENV NOTVISIBLE="in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]