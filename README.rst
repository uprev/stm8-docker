STM8-Docker
===========

This repo contains the docker file for the the docker image uprev/stm8. The image has the toolchain and libraries to build firmware for stm8 microcontrollers. 

The image can be built with '--target dev' for a smaller image with all of the tools for local development, or built completely to include dependencies to be used as a jenkins build node.

devcontainer
------------

.devcontainer contains the VS Code devcontainer configuration for stm8 projects.


StdPeriph_Lib
-------------

The standard peripheral library for STM8 mcus is located at /opt/STM8S_StdPeriph_Lib

